#include <iostream>
#include <vector>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Oh noes it cannot be" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!" << std::endl;
	}
		
};

class Duck : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Quack!" << std::endl;
	}
};

int main()
{
	std::vector<std::unique_ptr<Animal>> animals{};

	animals.push_back(std::unique_ptr<Animal>(new Dog()));
	animals.push_back(std::unique_ptr<Animal>(new Cat()));
	animals.push_back(std::unique_ptr<Animal>(new Duck()));
	animals.push_back(std::unique_ptr<Animal>(new Animal()));

	for (std::unique_ptr<Animal>& animal : animals) // <- ��������, ��� ��� ����� ���� ������� ����� ���������� ����.
	{
		std::cout << "an animal goes ";
		animal->Voice();
	}
}